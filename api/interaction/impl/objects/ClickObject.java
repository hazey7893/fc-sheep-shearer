package scripts.fc.api.interaction.impl.objects;

import org.tribot.api.DynamicClicking;
import org.tribot.api2007.types.RSObject;

import scripts.fc.api.interaction.ObjectInteraction;

public class ClickObject extends ObjectInteraction
{
	public ClickObject(String action, String name, int searchDistance)
	{
		super(action, name, searchDistance);
	}
	
	public ClickObject(String action, int id, int searchDistance)
	{
		super(action, id, searchDistance);
	}
	
	public ClickObject(String action, RSObject object)
	{
		super(action, object);
	}

	@Override
	protected boolean interact()
	{
		return DynamicClicking.clickRSObject(object, action);
	}

}
