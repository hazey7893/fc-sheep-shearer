package scripts.fc.api.generic;

import org.tribot.api.General;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSCharacter;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSObject;

public class FCConditions
{
	public static final Condition IN_BANK_CONDITION = inBankCondition();
	public static final Condition IN_DIALOGUE_CONDITION = inDialogueCondition();
	public static final Condition DEPOSIT_BOX_OPEN_CONDITION = depositBoxOpenCondition();
	public static final Condition CLICK_CONTINUE_CONDITION = clickContinueCondition();
	public static final Condition KILL_CONDITION = killCondition();
	public static final Condition IN_GAME_CONDITION = inGameCondition();
	public static final Condition NOT_IN_GAME_CONDITION = notInGameCondition();
	public static final Condition BANK_LOADED_CONDITION = bankLoadedCondition();
	public static final Condition IN_COMBAT_CONDITION = inCombatCondition();
	public static final Condition ENTER_AMOUNT_CONDITION = enterAmountCondition();
	
	
	public static Condition objectOnScreenCondition(final RSObject object)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return object.isOnScreen();
			}
		};
	}
	
	public static Condition settingEqualsCondition(final int index, final int setting)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Game.getSetting(index) == setting;
			}
		};
	}
	
	private static Condition enterAmountCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				RSInterface inter = Interfaces.get(162, 32);
				return inter != null && !inter.isHidden();
			}
		};
	}
	
	private static Condition inCombatCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Player.getRSPlayer().isInCombat();
			}
		};
	}
	
	private static Condition bankLoadedCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Banking.getAll().length > 0;
			}
		};
	}
	
	private static Condition clickContinueCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return NPCChat.getClickContinueInterface() != null;
			}
		};
	}
	
	private static Condition notInGameCondition()
	{
		return new Condition()
		{
			public boolean active()
			{
				General.sleep(100);
				return Game.getGameState() != 30 || Login.getLoginState() != STATE.INGAME;
			}
		};
	}
	
	private static Condition inGameCondition()
	{
		return new Condition()
		{
			public boolean active()
			{
				General.sleep(100);
				return Login.getLoginState() == STATE.INGAME
						&& Game.getGameState() == 30;
			}
		};
	}
	
	public static Condition interfaceUp(int id)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Interfaces.get(id) != null;
			}

		};
	}
	
	public static Condition killCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				RSCharacter target = Combat.getTargetEntity();
				RSCharacter rangedTarget = Player.getRSPlayer().getInteractingCharacter();
				return target != null && target.getHealth() <= 0 ||
						(rangedTarget != null && rangedTarget.isInCombat()
						&& rangedTarget.isInteractingWithMe() && rangedTarget.getHealth() <= 0);
			}
	
		};
	}
	
	private static Condition inDialogueCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);		
				return NPCChat.getClickContinueInterface() != null || NPCChat.getSelectOptionInterface() != null;
			}
		};
	}
	
	public static Condition positionEquals(Positionable p)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Player.getPosition().equals(p);
			}
		};
	}
	
	private static Condition inBankCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);				
				return Banking.isInBank();
			}
			
		};
	}
	
	private static Condition depositBoxOpenCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Banking.isDepositBoxOpen();
			}
			
		};
	}
	
	public static Condition planeChanged(final int startingPlane)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Player.getPosition().getPlane() != startingPlane;
			}
		};
	}
	
	public static Condition animationChanged(final int startingAnimation)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				
				return Player.getAnimation() != startingAnimation;
			}	
		};
	}
	
	public static Condition inventoryContains(final String name)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Inventory.getCount(name) > 0;
			}
		};
	}
	
	public static Condition inAreaCondition(final RSArea area)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{	
				/*
				 * No sleep because this is being used for walking methods 
				 * (which have the sleep as a param)
				 */
				return area.contains(Player.getPosition());
			}
			
		};
	}
	
	public static Condition inventoryChanged(final int startingAmt)
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100);
				return Inventory.getAll().length != startingAmt;
			}	
		};
	}
}
