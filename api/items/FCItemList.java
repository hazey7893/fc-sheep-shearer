package scripts.fc.api.items;

import java.util.ArrayList;
import java.util.Arrays;

import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;

import scripts.fc.api.generic.FCConditions;

public class FCItemList extends ArrayList<FCItem>
{
	private static final long serialVersionUID = -5556404383763797322L;

	public FCItemList(FCItem... items)
	{
		addAll(Arrays.asList(items));
	}
	
	public boolean hasListInInv()
	{
		for(FCItem item : this)
			if(item.getWithdrawAmt() > 0)
				return false;
		
		return true;
	}
	
	public int getTotalInInventory()
	{
		int total = 0;
		for(FCItem item : this)
			total += item.getInvCount(item.isStackable());
		
		return total;
	}
	
	public int getTotalInvSpaceNeeded()
	{
		int total = 0;
		for(FCItem item : this)
			total += item.isStackable() ? 1 : item.getAmt();
		
		return total;
	}
	
	public int getRemainingCountNeeded()
	{
		int count = 0;
		for(FCItem item : this)
			count += item.getWithdrawAmt();
		
		return count;
	}
	
	public FCItem find(int id, String name)
	{
		for(FCItem item : this)
			if(item.getId() == id || (item.getName() != null && item.getName().equals(name)))
				return item;
		
		return null;
	}
	
	public boolean withdraw()
	{
		for(FCItem i : this)
		{
			if(i.getInvCount(false) >= i.getAmt())
				continue;
			
			final int INV_COUNT = Inventory.getAll().length;
			
			if(!i.withdraw(i.getWithdrawAmt()))
				return false;
			else
				Timing.waitCondition(FCConditions.inventoryChanged(INV_COUNT), 3500);
		}
		
		return true;
	}
	
}
