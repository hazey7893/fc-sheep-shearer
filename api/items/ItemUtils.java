package scripts.fc.api.items;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Game;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;

public class ItemUtils
{
	public static boolean useItemOnItem(String firstName, String secondName)
	{
		RSItem[] first = Inventory.find(firstName);
		RSItem[] second = Inventory.find(secondName);
		
		if(first.length > 0 && second.length > 0)
		{
			String upText = Game.getUptext();
			
			if(upText == null || !upText.contains(firstName))
			{
				if(first[0].click("Use"))
				{
					General.sleep(300, 600);
					
					upText = Game.getUptext();
					
					if(upText != null && upText.contains(firstName))
					{
						return Clicking.click(second[0]);
					}
				}
			}
			else
			{
				return Clicking.click(second[0]);
			}
		}
		
		return false;
	}
	
	public static boolean equipItem(String name)
	{
		if(GameTab.open(TABS.INVENTORY))
		{
			RSItem[] items = Inventory.find(name);
			
			if(items.length > 0)
			{
				RSItemDefinition def = items[0].getDefinition();
				
				if(def != null && def.getActions().length > 1 && Clicking.click(items[0]))
					General.sleep(300, 600);
			}
		}
		
		return Equipment.isEquipped(name);
	}
	
	public static boolean equipItem(int id)
	{
		if(GameTab.open(TABS.INVENTORY))
		{
			RSItem[] items = Inventory.find(id);
			
			if(items.length > 0)
			{
				RSItemDefinition def = items[0].getDefinition();
				
				if(def != null && def.getActions().length > 1 && Clicking.click(items[0]))
					General.sleep(300, 600);
			}
		}
		
		return Equipment.isEquipped(id);
	}
}
