package scripts.fc.api.banking;

import org.tribot.api.Clicking;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.items.FCItem;
import scripts.fc.api.items.FCItemList;

public class FCBanking
{
	private static final int MAX_INVENTORY_SIZE = 28;
	
	public static boolean withdraw(FCItemList itemList)
	{
		if(itemList.hasListInInv())
			return true;
		
		final int TOTAL_ITEMS_NEEDED = itemList.getRemainingCountNeeded();
		
		if(Inventory.getAll().length > (MAX_INVENTORY_SIZE - TOTAL_ITEMS_NEEDED)) //DOES NOT HAVE ENOUGH SPACE
			makeSpace(itemList);
		if(Inventory.getAll().length <= (MAX_INVENTORY_SIZE - TOTAL_ITEMS_NEEDED)) //HAS ENOUGH SPACE
			return itemList.withdraw();
			
		
		return false;
	}
	
	private static void makeSpace(FCItemList itemList)
	{
		for(RSItem invItem : Inventory.getAll())
		{
			if(Inventory.getCount(invItem.getID()) == 0)
				continue;
			
			final int ID = invItem.getID();
			final String NAME = invItem.getDefinition().getName();
			
			final FCItem LIST_ITEM = itemList.find(ID, NAME);
			
			if(LIST_ITEM != null)
				makeSpaceListItem(LIST_ITEM);
			else
				depositItem(invItem.getID(), 0);
					
		}
	}
	
	public static void depositItem(int id, int count)
	{
		final int INV_COUNT = Inventory.getCount(id);
		final int INV_SIZE = Inventory.getAll().length;
		boolean success = false;
		
		if(INV_COUNT == 1 && Clicking.click(Inventory.find(id)[0])) //LENGTH CHECK NOT NECESSARY
			success = true;
		else if(Banking.deposit(count, id))
			success = true;
				
		
		if(success)
			Timing.waitCondition(FCConditions.inventoryChanged(INV_SIZE), 3500);
	}
	
	private static void makeSpaceListItem(FCItem listItem)
	{
		final int OVERFLOW = listItem.getInvCount(listItem.isStackable()) - listItem.getRequiredInvSpace();
		final int INV_SIZE = Inventory.getAll().length;
		if(OVERFLOW > 0 && listItem.deposit(OVERFLOW))
			Timing.waitCondition(FCConditions.inventoryChanged(INV_SIZE), 3500);
	}
}
