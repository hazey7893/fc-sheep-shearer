package scripts.fc.api.viewport;

import org.tribot.api.General;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Camera.ROTATION_METHOD;
import org.tribot.api2007.Player;
import org.tribot.script.Script;
/**
 * 
 * @author Final Calibur & WastedBro (no specific order)
 *
 */
public class ACamera 
{
	//Local constants
	private final int ROTATION_THRESHOLD 	= 30;	//We will not change the rotation when turning to a tile if the current rotation is +- this amount from the optimal value
	private final int ANGLE_THRESHOLD		= 15; 	//Same as above, but for angle
	
    //Private instance fields
    private RotationThread          rotationThread;	//The thread that will handle camera rotation
    private AngleThread             angleThread;	//The thread that will handle camera angle 
    private Script					script;			//The script we are handling the camera for. This is so we can know when to shut off the threads.
    private boolean 				runsWithoutScript;
    
    /**
     * Default constructor.
     */
    public ACamera(Script s)
    {
        instantiateVars(s);
    }
    
    public ACamera()
    {
    	this.runsWithoutScript = true;
    	
    	instantiateVars(null);
    }
    
    /**
     *	This method instantiates all of our variables for us.
     *	I decided to make this method because it makes instantiating the variables in the constructors less redundant.
     *
     * 	@param s script we are controlling camera for
     * 	@param sleepTime sleep time between cycles
     */
    private void instantiateVars(Script s)
    {
    	this.script = s;
        this.rotationThread = new RotationThread();
        this.rotationThread.setName("ACamera Rotation Thread");
        this.angleThread = new AngleThread();
        this.angleThread.setName("ACamera Angle Thread");
        this.rotationThread.start();
        this.angleThread.start();
        Camera.setRotationMethod(ROTATION_METHOD.ONLY_KEYS);
    }
    
    /**
     * Sets the camera angle
     * @param specified angle to set camera to
     */
    public void setCameraAngle(int angle)
    {
    	synchronized(this.angleThread)
    	{
    		this.angleThread.sleepTime = 0;
    		this.angleThread.angle = angle;
    		this.angleThread.notify();
    	}
    }
    
    /**
     * Sets the camera angle
     */
    public void setCameraAngle(int angle, long sleepTime)
    {
    	synchronized(this.angleThread)
    	{
    		this.angleThread.sleepTime = sleepTime;
    		this.angleThread.angle = angle;
    		this.angleThread.notify();
    	}
    }
    
    /**
     * Sets the camera rotation
     * @param rotation specified rotation to set camera to
     */
    public void setCameraRotation(int rotation)
    {
    	synchronized(this.rotationThread)
    	{
	        this.rotationThread.rotation = rotation;
	        this.rotationThread.notify();
    	}
    }
    
    /**
     * 	Turns the camera to a specific tile.
     * 
     * 	@param tile to turn to
     */
    public void turnToTile(Positionable tile)
    {  
        int optimalAngle = adjustAngleToTile(tile);
        int optimalRotation = Camera.getTileAngle(tile);
        
        if(Math.abs(optimalAngle - Camera.getCameraAngle()) > ANGLE_THRESHOLD)
        {
        	setCameraAngle(optimalAngle + General.random(-12, 12), calculateSleepTime());
        }
        
        if(Math.abs(optimalRotation - Camera.getCameraRotation()) > ROTATION_THRESHOLD)
        {
        	setCameraRotation(optimalRotation + General.random(-30, 30));
        }        
    }
    
    private long calculateSleepTime()
    {
    	int diff	= Math.abs(Camera.getCameraRotation() - rotationThread.rotation);
    	int minTime = (int)(diff * 2.0);
    	int maxTime = (int)(diff * 3.0);
    	
    	return (General.random(minTime, maxTime));
    }
    
    /**
     * Adjusts the angle of the camera so that it is optimal for the tile
     * @param tile Tile to adjust angle for
     * @return the optimal angle to view this tile at
     */
    public int adjustAngleToTile(Positionable tile)
    {
    	//Distance from player to object - Used in calculating the optimal angle.
    	//Objects that are farther away require the camera to be turned to a lower angle to increase viewing distance.
    	int distance = Player.getPosition().distanceTo(tile);
    	
    	//The angle is calculated by taking the max height (100, optimal for very close objects),
    	//and subtracting an arbitrary number (I chose 6 degrees) for every tile that it is away.
    	int angle = 100 - (distance * 6);
    	
    	return angle;
    }
    
    private class RotationThread extends Thread
    {	
        protected int rotation = Camera.getCameraRotation();
        
        @Override
        public synchronized void run() 
        {
        	try
        	{  
        		while(script != null || runsWithoutScript)
        		{       		
		            Camera.setCameraRotation(rotation);
		               
		            getRotationThread().wait();
        		}
        	}
        	catch(Exception e)
        	{
        		General.println("Error initiating wait on angle thread.");
        	}
        }
        
    }
       
    private class AngleThread extends Thread
    {
        protected int 	angle 		= Camera.getCameraAngle();
        protected long 	sleepTime	= 0;
        		
        @Override
        public synchronized void run() 
        {
        	try
        	{   
        		while(script != null || runsWithoutScript)
        		{
        			sleep(sleepTime);
        			
		            Camera.setCameraAngle(angle);
		               
		            getAngleThread().wait();      
        		}
        	}
        	catch(Exception e)
        	{
        		General.println("Error initiating wait on angle thread.");
        	}
        }
        
    }

    public RotationThread getRotationThread() 
    {
        return rotationThread;
    }

    public AngleThread getAngleThread() 
    {
        return angleThread;
    }
  
    public Script getScript() {
		return script;
	}
    
}