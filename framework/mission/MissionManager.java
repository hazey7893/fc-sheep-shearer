package scripts.fc.framework.mission;

import scripts.fc.framework.script.FCScript;
import scripts.fc.framework.task.TaskManager;

public abstract class MissionManager extends TaskManager implements Mission
{
	private boolean hasAddedPreReqs;
	
	public MissionManager(FCScript script)
	{
		super(script);
	}
	
	public MissionManager()
	{
		super();
	}
	
	public boolean hasAddedPreReqs()
	{
		return hasAddedPreReqs;
	}
	
	public void setHasAddedPreReqs(boolean b)
	{
		hasAddedPreReqs = b;
	}
}
